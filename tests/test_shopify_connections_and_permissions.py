import os
import requests
from requests.api import get
import yaml

#Get project root path
ROOT_DIR = os.path.dirname(os.path.abspath(__file__ + "/..")) # This is your Project Root
DATA_DIR = os.path.join(ROOT_DIR, 'data')

# load yml file to dictionary
settings = yaml.load(open(os.path.join(ROOT_DIR, 'settings.yaml')), Loader=yaml.FullLoader)
stores = settings['stores']

test_graphQLquery = """{
  appInstallation {
    accessScopes{handle}
  }
}
"""

#Shopify Credentials and Settings
def get_shop_url_with_credentials(store):
    # load yml file to dictionary
    credentials = yaml.load(open(os.path.join(ROOT_DIR, 'credentials.yaml')), Loader=yaml.FullLoader)

    # access values from dictionary
    API_KEY =  credentials[store]['api_key']
    PASSWORD =  credentials[store]['password']
    SHOP_NAME =  credentials[store]['shop_name']
    API_VERSION =  credentials[store]['api_version']

    return "https://%s:%s@%s.myshopify.com/admin/api/%s" % (API_KEY, PASSWORD, SHOP_NAME, API_VERSION)

def test_store_connection_staging():
    store = 'shopify_staging_4'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True

def test_store_connection_dach():
    store = 'shopify_dach'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True

def test_store_connection_es():
    store = 'shopify_es'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True

def test_store_connection_eu():
    store = 'shopify_eu'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True
        
def test_store_connection_fr():
    store = 'shopify_fr'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True

def test_store_connection_it():
    store = 'shopify_it'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True

def test_store_connection_nl():
    store = 'shopify_nl'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True

def test_store_connection_uk():
    store = 'shopify_uk'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True

def test_store_connection_us():
    store = 'shopify_us'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True

def test_store_connection_cz():
    store = 'shopify_cz'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True

def test_store_connection_pl():
    store = 'shopify_pl'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True

def test_store_connection_sk():
    store = 'shopify_sk'
    print(f'STORE: {store}')
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': test_graphQLquery})
    assert response.status_code == 200
    result = response.json()
    permissions = []
    for perm in result["data"]["appInstallation"]["accessScopes"]:
        permissions.append(perm['handle'])
    read_orders = "read_orders" in permissions 
    assert read_orders == True
    read_cust = "read_customers" in permissions
    assert read_cust == True
    write_cust = "write_customers" in permissions
    assert write_cust == True