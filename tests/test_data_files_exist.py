import os

#Get project root path
ROOT_DIR = os.path.dirname(os.path.abspath(__file__ + "/..")) # This is your Project Root
DATA_DIR = os.path.join(ROOT_DIR, 'data/')
tags_pps_file = 'waterdrop-export.csv'
tags_unified = 'waterdrop-tags-unified.csv'

def test_unified_tags_file_exist():
    file = os.path.join(DATA_DIR + tags_unified)
    file_exists = os.path.exists(file)
    assert file_exists == True

def test_export_file_exist():
    file = os.path.join(DATA_DIR + tags_pps_file)
    file_exists = os.path.exists(file)
    assert file_exists == True
