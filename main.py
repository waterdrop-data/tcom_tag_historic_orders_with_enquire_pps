import json
import time
import requests
import pandas as pd 
import os
import yaml
import re

#Environment
STAGE = 'DEV'

#Get project root path
ROOT_DIR = os.path.dirname(os.path.abspath(__file__)) # This is your Project Root
DATA_DIR = os.path.join(ROOT_DIR, 'data/')
tags_pps_file = 'waterdrop-export.csv'
tags_unified = 'waterdrop-tags-unified.csv'

#Shopify Credentials and Settings
def get_shop_url_with_credentials(store):
    # load yml file to dictionary
    credentials = yaml.load(open(os.path.join(ROOT_DIR, 'credentials.yaml')), Loader=yaml.FullLoader)

    # access values from dictionary
    API_KEY =  credentials[store]['api_key']
    PASSWORD =  credentials[store]['password']
    SHOP_NAME =  credentials[store]['shop_name']
    API_VERSION =  credentials[store]['api_version']

    return "https://%s:%s@%s.myshopify.com/admin/api/%s" % (API_KEY, PASSWORD, SHOP_NAME, API_VERSION)


def call_shopify_graphql(GraphQLString, store):
    shop_url = get_shop_url_with_credentials(store)
    response = requests.post(shop_url+'/graphql.json', json={'query': GraphQLString})
    if response.status_code==400:
        raise ValueError('GraphQL error:' + response.text)
    answer = json.loads(response.text)
    throttleStatus = None
    if 'errors' in answer:
        try:
            throttleStatus = answer['errors'][0]['extensions']['code']
        except:
            pass
        if throttleStatus != 'THROTTLED':
            raise ValueError('GraphQL error:' + repr(answer['errors']))
    qlRequired = answer['extensions']['cost']['requestedQueryCost']
    qlLimit = answer['extensions']['cost']['throttleStatus']['maximumAvailable']
    qlAvail = answer['extensions']['cost']['throttleStatus']['currentlyAvailable']
    print('GraphQL throttling status: ' + str(qlAvail) + '/' + str(int(qlLimit)))
    while throttleStatus=='THROTTLED':
        print('Waiting (GraphQL throttling)... ' + str(qlAvail) + '/' + str(int(qlLimit)) + ' used, requested ' + str(qlRequired))
        time.sleep(1)
        response = requests.post(shop_url+'/graphql.json', json={'query': GraphQLString})
        answer = json.loads(response.text)
        try: 
            throttleStatus = answer['errors'][0]['extensions']['code']
        except:
            throttleStatus = None
            pass
    return answer['data']

def read_unified_definition_table(filename) -> pd.DataFrame:
    df_tags_unified = pd.read_csv(os.path.join(DATA_DIR, filename), sep=';',encoding='latin1')
    df_tags_unified.columns = ['shopify_domain', 'question' ,'question_id', 'response', 'en_tags', 'local_language_tags']
    df_tags_unified['response'] = df_tags_unified['response'].map(lambda x: str(x))
    df_tags_unified['response'] = df_tags_unified['response'].map(lambda x: re.sub(r'\W+', '', str(x)))
    df_tags_unified['response'] = df_tags_unified['response'].map(lambda x: x.lower())
    df_tags_unified['response'] = df_tags_unified['response'].map(lambda x: x.strip())
    # df_tags_unified.to_csv(os.path.join(DATA_DIR + 'df_tags_unified.csv'))
    return df_tags_unified

def read_exported_pps_file(filename) -> pd.DataFrame:
    df_pps = pd.read_csv(os.path.join(DATA_DIR, filename))
    df_pps.columns = ['shopify_domain','shop_id','order_id','order_number','response']
    df_pps['response'] = df_pps['response'].map(lambda x: str(x))
    df_pps['response'] = df_pps['response'].map(lambda x: re.sub(r'\W+', '', str(x)))
    df_pps['response'] = df_pps['response'].map(lambda x: x.lower())
    df_pps['response'] = df_pps['response'].map(lambda x: x.strip())
    # df_pps.to_csv(os.path.join(DATA_DIR + 'df_pps.csv')) 
    return df_pps

def join_tags_and_export_df(df_pps:pd.DataFrame, df_tags_unified:pd.DataFrame) -> pd.DataFrame():
    df_combined = pd.merge(df_pps,df_tags_unified, how='left', on=['shopify_domain', 'response'])
    df_combined = df_combined.loc[:,['shopify_domain','shop_id','order_id','order_number','response','en_tags', 'local_language_tags']]
    df_combined.to_csv(os.path.join(DATA_DIR + 'df_combined.csv'))

    df_no_match = (df_combined[df_combined['en_tags'].isnull()])
    df_no_match.to_csv(os.path.join(DATA_DIR + 'df_NO_MATCH.csv'))
    df_no_match_agg = df_no_match.groupby(['shopify_domain','response']).size().reset_index(name='counts').sort_values('counts', ascending=False).reset_index(drop=True)
    df_no_match_agg.to_csv(os.path.join(DATA_DIR + 'df_no_match_agg.csv'))
    
    return df_combined

def main():
    #df_pps = read_and_join_files()
    df_unified_definition = read_unified_definition_table(tags_unified)
    df_exported_pps_data = read_exported_pps_file(tags_pps_file)
    df_combined = join_tags_and_export_df(df_exported_pps_data, df_unified_definition)
    
    for store in stores:
        graph = call_shopify_graphql(BULK_graphQLquery, store)
        print(graph)
        finish = False
        while not finish:
            time.sleep(5)
            response_status = call_shopify_graphql(BULK_status, store)
            status = response_status["currentBulkOperation"]["status"]
            if status in ['CANCELED', 'COMPLETED','FAILED']:
                finish = True
                url = response_status["currentBulkOperation"]["url"]
        r = requests.get(url, allow_redirects=True)
        open(os.path.join(DATA_DIR, f'orders_{store}.jsonl'), 'wb').write(r.content)

if __name__ == '__main__':
    # load yml file to dictionary

    #TODO: Tag geiler_kollege im Test staging4

    #TODO: Test - Tag schon vorhanden

    
    settings = yaml.load(open(os.path.join(ROOT_DIR, 'settings.yaml')), Loader=yaml.FullLoader)

    graphQLquery = """{
    orders(first: 10, query:"id:1861980848246") {
        edges {
        node {
            id
            name
            customer{
                id
            }
        }
        }
    }
    }"""

    BULK_graphQLquery = '''
        mutation {
        bulkOperationRunQuery(
        query:"""
        {
        orders(query: "created_at:>=2021-01-01") {
            edges {
            node {
                id
                name
                customer{
                    id
                }
            }
            }
        }
        }
        """
    ) {
        bulkOperation {
        id
        status
        }
        userErrors {
        field
        message
        }
    }
    }
    '''

    BULK_status = '''
        {
    currentBulkOperation {
        id
        status
        errorCode
        createdAt
        completedAt
        objectCount
        fileSize
        url
        partialDataUrl
    }
    }
    '''

    if STAGE == "PROD":
        stores = settings['stores']
    else:
        stores = ['shopify_fr']
    main()
    